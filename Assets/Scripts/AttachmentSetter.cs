﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class AttachmentSetter : MonoBehaviour
{
    public SkeletonAnimation SkeletonAnimation;

    [SpineSlot(dataField = "SkeletonAnimation")]
    public string WeaponSlot;

    [SpineAttachment(dataField = "SkeletonAnimation", slotField = "WeaponSlot")]
    public string[] Attachments;

    public void SetAttachment(int index)
    {
        if (index < 0 || index >= Attachments.Length)
        {
            SkeletonAnimation.Skeleton.SetAttachment(WeaponSlot, null);
            return;
        }

        SkeletonAnimation.Skeleton.SetAttachment(WeaponSlot, Attachments[index]);
    }
}