﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
	Camera _camera;
	private void Start()
	{
		_camera = Camera.main;
	}

	private void OnMouseDrag()
	{
		Vector3 pos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);
		Vector3 worldPos = _camera.ScreenToWorldPoint(pos);
		transform.position = worldPos;
	}
}