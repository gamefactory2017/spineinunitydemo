﻿using Spine.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class SpineToUnityEvent : MonoBehaviour {

	public SkeletonAnimation SkeletonAnimation;
	
	[SpineEvent(dataField: "SkeletonAnimation")] 
	public string SpineEvent;
	
	public UnityEvent OnEvent;

	void Start () {
		
		Assert.IsNotNull(SkeletonAnimation);
		
		// This is how you subscribe via a declared method. The method needs the correct signature.
		SkeletonAnimation.state.Event += HandleEvent;
	}

	void HandleEvent (Spine.TrackEntry entry, Spine.Event e)
	{

		//Skip event handling if disabled
		if (!enabled)
			return;
		
		if (e.Data.Name == SpineEvent) {         
		//Event we were looking for
			OnEvent.Invoke();
		}
	}
}
