﻿using Spine.Unity;
using UnityEngine;

public class Enemy : MonoBehaviour , IHitable
{
	public SkeletonAnimation SkeletonAnimation;
	public Rigidbody Rigidbody;

	public Transform target;
	
	[Header("Moving")]
	public float runSpeed = 1f;
	
	[Header("Animations")]
	[SpineAnimation(dataField: "SkeletonAnimation")]
	public string flyName = "Run";

	// Update is called once per frame
	void Update ()
	{
		var velocity =(target.position - transform.position).normalized * runSpeed ;
		Rigidbody.velocity = velocity;
		if (velocity.x != 0)
			SkeletonAnimation.Skeleton.FlipX = velocity.x > 0;
	}

	public void Hit()
	{
		if (!enabled) 
			return;
		
		gameObject.SetActive(false);
	}
}
