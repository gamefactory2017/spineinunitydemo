﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float Speed = 1;
	public Vector3 Direction = Vector3.zero;

	private float FlyTime = 5f;

	public GameObject HitEffect;
	
	// Update is called once per frame
	void Update ()
	{
		if (FlyTime <= 0)
		{
			Disable();
			return;
		}
		FlyTime -= Time.deltaTime;
		
		transform.Translate(Direction.normalized * Speed * Time.deltaTime);
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.isStatic)
			Disable();
		
		var hitable = other.GetComponent<IHitable>();
		if (hitable == null) 
			return;
		
		hitable.Hit();
		Instantiate(HitEffect).transform.position =  transform.position;
		
		Disable();
	}

	private void Disable()
	{
		if(enabled)
			gameObject.SetActive(false);
	}
}
