﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySetter : MonoBehaviour
{
    public Transform target;

    public void setUp(GameObject enemy)
    {
        enemy.GetComponent<Enemy>().target = target;
    }
}
