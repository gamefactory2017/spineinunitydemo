﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
	public float Delay = 1;

	// Use this for initialization
	void Start()
	{
		Destroy(gameObject, Delay);
	}
}
