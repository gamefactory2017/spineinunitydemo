﻿using DG.Tweening;
using Spine.Unity;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public Vector3 ShootStartPosition
    {
        get
        {
            Vector3 tmp = Vector3.right;
            if (skeletonAnimation.skeleton.flipX)
                tmp *= -1;
            
            return transform.position + Vector3.up + tmp;
        }
    }

    public Gun gunSettings;
    public Gun gunSettings2;

    private Gun _currentGunSettings;
    [Header("Weapons")] 
    [SpineSlot(dataField = "skeletonAnimation")] 
    public string WeaponSlot;

    public SkeletonAnimation skeletonAnimation;

    private Transform BulletHolder;

    private void Start()
    {
        BulletHolder = new GameObject("BulletHoldere").transform;
        EquipWeapon(gunSettings);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var bullet = GameObject
                .Instantiate(_currentGunSettings.Bullet, ShootStartPosition, Quaternion.identity)
                .GetComponent<Bullet>();
            bullet.Direction = Vector3.right * (skeletonAnimation.Skeleton.FlipX ? -1 : 1);
            bullet.transform.SetParent(BulletHolder);

            ShootEffect();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            EquipWeapon(gunSettings);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            EquipWeapon(gunSettings2);
        }
    }

    private void EquipWeapon(Gun p0)
    {
        skeletonAnimation.skeleton.SetAttachment(WeaponSlot, p0.WeaponItem);
        _currentGunSettings = p0;
    }

    private void ShootEffect()
    {
        Camera.main.DOShakePosition(0.2f, 0.1f, 1);
    }
}