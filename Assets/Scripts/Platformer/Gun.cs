﻿using Spine.Unity;
using UnityEngine;

[CreateAssetMenu(fileName = "Gun")]
public class Gun : ScriptableObject
{
    public SkeletonDataAsset skeletonAnimation;
    [SpineAttachment(dataField = "skeletonAnimation")]
    public string WeaponItem;
    public GameObject Bullet;
}
