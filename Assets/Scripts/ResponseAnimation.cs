﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class ResponseAnimation : MonoBehaviour
{
    public SkeletonAnimation SkeletonAnimation;
    public PlayAnimation PlayAnimation;
    [SpineAnimation(dataField = "SkeletonAnimation")]
    public string animationName;

    public void Response()
    {
        PlayAnimation.PlayOnce(animationName);
    }
}