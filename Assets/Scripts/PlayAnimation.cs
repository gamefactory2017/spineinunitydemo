﻿using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

public class PlayAnimation : MonoBehaviour
{
    public SkeletonAnimation SkeletonAnimation;

    [SpineAnimation(dataField = "SkeletonAnimation")]
    public string[] Animations;
    public bool IsLoop;

    private int _currentAnimation;
    private TrackEntry _currentTrack;

    public void PlayNextAnimation()
    {
        int next = _currentAnimation != Animations.Length - 1 ? (_currentAnimation + 1) : 0;
        Play(next);
    }

    public void PlayOnce(string animationName)
    {
        SkeletonAnimation.AnimationState.SetAnimation(0, animationName, false).Complete += (track) =>
        {
            Play(_currentAnimation);
        };
    }

    #region MonoBehaviour methods
    private void Start()
    {
        Play(0);
    }
    #endregion

    #region Private methods
    private void Play(int index)
    {
        if (index < 0 || index >= Animations.Length)
            return;

        _currentTrack = SkeletonAnimation.AnimationState.SetAnimation(0, Animations[index], IsLoop);
        _currentTrack.Complete += (track) =>
        {
            _currentTrack = null;
        };
        _currentAnimation = index;
    }
    #endregion

    #region Event handlers
    public void OnSliderValueChanged_handler(float value)
    {
        SkeletonAnimation.timeScale = value;
    }

    public void OnToggleValueChanged_handler(bool value)
    {
        IsLoop = value;
        if (_currentTrack != null)
            _currentTrack.Complete += (track) => Play(_currentAnimation);
        else
            Play(_currentAnimation);
    }
    #endregion
}