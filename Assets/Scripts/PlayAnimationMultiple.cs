﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimationMultiple : MonoBehaviour
{
	public PlayAnimation[] PlayAnimations;

	public void PlayNextAnimation()
	{
		for (int i = 0; i < PlayAnimations.Length; i++)
		{
			PlayAnimations[i].PlayNextAnimation();
		}
	}

	#region Events Handlers
	public void OnSliderValueChanged_handler(float value)
	{
		for (int i = 0; i < PlayAnimations.Length; i++)
		{
			PlayAnimations[i].OnSliderValueChanged_handler(value);
		}
	}

	public void OnToggleValueChanged_handler(bool value)
	{
		for (int i = 0; i < PlayAnimations.Length; i++)
		{
			PlayAnimations[i].OnToggleValueChanged_handler(value);
		}
	}
	#endregion
}