﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class SkinChanger : MonoBehaviour
{
    public SkeletonAnimation SkeletonAnimation;

    [SpineSkin(dataField = "SkeletonAnimation")]
    public string[] Skins;

    private int _currentSkin;

    public void ChangeNextSkin()
    {
        int next = _currentSkin + 1 >= Skins.Length ? 0 : _currentSkin + 1;
        ChangeSkin(next);
    }

    public void ChangeSkin(string skinName)
    {
        SkeletonAnimation.Skeleton.SetSkin(skinName);
    }

    private void ChangeSkin(int index)
    {
        if (index < 0 || index >= Skins.Length)
            return;

        ChangeSkin(Skins[index]);
        _currentSkin = index;
    }
}