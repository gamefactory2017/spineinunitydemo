﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SkeletonDamageResponse : MonoBehaviour
{
    public Renderer Renderer;
    public float BlinkPerSecond = 2f;
    public float Duration = 3f;

    [Header("Material data")]
    [Range(-0.5f, 0.5f)]
    public float Hue = 0f;
    [Range(0, 2)]
    public float Saturation = 1f;
    [Range(0, 2)]
    public float Brightness = 1f;
    public Color OverlayColor = Color.clear;

    private float _countTime;
    private float _countBlink;
    private float _blinkTriggerTime;
    private bool _isRunning;
    private bool _isBlink;

    private MaterialPropertyBlock _mpb;

    [ContextMenu("Blink")]
    public void Blink()
    {
        _countBlink = 0f;
        _countTime = 0f;
        _isBlink = false;
        _blinkTriggerTime = 1.0f / BlinkPerSecond;
        _isRunning = true;
    }

    private void Start()
    {
        _mpb = new MaterialPropertyBlock();
    }

    private void Update()
    {
        if (_isRunning == false)
            return;

        _countBlink += Time.deltaTime;
        _countTime += Time.deltaTime;

        if (_countBlink >= _blinkTriggerTime)
        {
            ToggleBlinkState();
            _countBlink = 0f;
        }

        if (_countTime >= Duration)
        {
            ResetBlinkState();
            _isRunning = false;
        }
    }

    private void SetBlinkState()
    {
        Renderer.GetPropertyBlock(_mpb);
        _mpb.SetColor("_OverlayColor", OverlayColor);
        _mpb.SetFloat("_Hue", Hue);
        _mpb.SetFloat("_Saturation", Saturation);
        _mpb.SetFloat("_Brightness", Brightness);
        Renderer.SetPropertyBlock(_mpb);
        _isBlink = true;
    }

    private void ResetBlinkState()
    {
        Renderer.GetPropertyBlock(_mpb);
        _mpb.SetColor("_OverlayColor", Color.clear);
        _mpb.SetFloat("_Hue", 0f);
        _mpb.SetFloat("_Saturation", 1f);
        _mpb.SetFloat("_Brightness", 1f);
        Renderer.SetPropertyBlock(_mpb);
        _isBlink = false;
    }

    private void ToggleBlinkState()
    {
        if (_isBlink)
            ResetBlinkState();
        else
            SetBlinkState();
    }
}