﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
	[Range(0, 50)] 
	public int count = 50;
	public GameObject Dummy;
	public float delay = 0.2f;
	public float Range = 6;

	public SpawnedEvent onSpawned;
	
	// Use this for initialization
	IEnumerator Start () {

		for (int i = 0; i < 50; i++)
		{
			var obj = GameObject.Instantiate(Dummy);
			obj.transform.SetParent(transform);
			obj.transform.position = transform.position +  (Vector3)(Random.insideUnitCircle * Range);
			
			onSpawned.Invoke(obj);
			
			yield return  new WaitForSeconds(delay);
		}
	}
}


[System.Serializable]
public class SpawnedEvent : UnityEvent<GameObject>
{
}