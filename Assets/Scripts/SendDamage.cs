﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendDamage : MonoBehaviour
{
	public GameEvent OnDamageEvent;
	private void OnTriggerEnter2D(Collider2D other)
	{
		var listener = other.GetComponent<GameEventListener>();
		if (listener != null && listener.Event == OnDamageEvent)
		{
			listener.Event.Raise();
		}
	}
}