﻿using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

public class IKFollower : MonoBehaviour
{
    public SkeletonAnimation SkeletonAnimation;
    public Transform TransformToFollow;

    [SpineIkConstraint(dataField = "SkeletonAnimation")]
    public string IKConstraintName;

    private IkConstraint _constraint;

    private void Start()
    {
        _constraint = SkeletonAnimation.Skeleton.FindIkConstraint(IKConstraintName);
        _constraint.Mix = 1f;
    }

    private void Update()
    {
        _constraint.Target.SetPosition(TransformToFollow.position);
    }
}